package controllerTest;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.File;

import org.testng.annotations.Test;

import controller.Verificador;
import controller.Verificador.*;
import model.Aula;

public class CriterioAceptacion2 extends BaseTest 
{
	
	
	@Test
	public void emptyFile() 
	{
		assertFalse (Verificador.archivoVacio(fileEmpty.objetosAula()));
	}
	
	@Test
	public void incompleteFile() 
	{
		assertFalse (Verificador.datosIncompletos(fileIncomplete.objetosAula()));
	}
		
}
