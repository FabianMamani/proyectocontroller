package controller;

public abstract class Verificador {
	
	public static boolean datosIncompletos(Object[] objetos)
	{
		boolean ret = false;
		
		for (Object obj : objetos)
		{
			if (objetoVacio(obj))
			{
				ret = true;
				break;
			}
		}
		
		return ret;
	}
	
	public static boolean archivoVacio(Object[] objetos)
	{
		boolean ret = true;
		for (Object obj : objetos)
		{
			if (!objetoVacio(obj)) {
				ret = ret && false;
				break;
			}
			
		}
		return ret;
	}
	
	private static boolean objetoVacio(Object obj) 
	{
		return obj.equals("");
	}

}
