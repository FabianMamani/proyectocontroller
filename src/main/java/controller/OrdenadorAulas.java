package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import json.Json;
import model.Aula;

public class OrdenadorAulas 
{
	ArrayList<Aula> _aulas;
	
	public OrdenadorAulas(ArrayList<Aula> aulas) 
	{
		_aulas = aulas;
	}
	
	public void ordenarAulas() 
	{
		Collections.sort(_aulas,Edificio());
	}

	private Comparator<Aula> Edificio() {
		return (uno,otro) -> uno.get_edificio().compareTo(otro.get_edificio());
	}
	
	public ArrayList<Aula> obtenerAulas()
	{
		return _aulas;
	}
	
	public void mostrarAulas()
	{
		for(Aula aula : _aulas) {
			System.out.println(aula.toString());
		}
	}
	

}
