package json;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Aula;

public class Json {
	private ArrayList<Aula>  _aulas;
	
	public Json()
	{
		_aulas = new ArrayList<Aula>();
		
	}
	
	public void AgregarAula(String id, int capacidad, String edificio,String nombre,boolean habilitado,String tipo) {
		_aulas.add(new Aula(id,capacidad,edificio,nombre,habilitado,tipo));
	}
	
	public ArrayList<Aula> ObtenerAulas(){
		return _aulas;
	}
	
	public Object[] objetosAula()
	{
		return _aulas.toArray();
	}

	public void generarJson(String archivo)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		
		try {
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static Json leerJson(String archivo)
	{
		Gson gson = new Gson();
		Json ret = null;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));		
			ret = gson.fromJson(br, Json.class);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return ret;
	}
	
	public void cargar(Json archivo) {
		archivo.AgregarAula("A001", 40, "Ciencias", "Laboratorio II", true, "Laboratorio");
		archivo.AgregarAula("A002", 35, "Derecho", "Derechos Humanos", true, "Aula");
		archivo.AgregarAula("A003", 30, "Arquitectura", "Proyectual", true, "Aula");
		archivo.AgregarAula("A004", 50, "Medicina", "Anatomia", true, "Aula");
		archivo.AgregarAula("A005", 20, "Ciencias", "Algoritmos II", true, "Laboratorio");
	}
	
}
